<?php

include("config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM siswa WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}


function kelas($data) {
    if($data == 0 ) {
        echo 'X RPL';
    }
    if($data == 1 ) {
        echo 'XI RPL';
    }
    if($data == 2 ) {
        echo 'XII RPL';
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Belajar CRUD</title>
</head>

<body>
    <h3>Detail Data</h3>
    <form action="update-proses.php" method="POST">
        <fieldset>
            <table>
                <tr>
                    <td><label for="nisn">NISN: <?=$row['nisn']?> </label></td>
                </tr> 
                <tr>
                    <td><label for="nis">NIS: <?=$row['nis']?> </label></td>
                </tr>
                <tr>
                    <td><label for="nama">Nama: <?= $row['nama']?>  </label></td>
                </tr>
                <tr>
                <td><label for="kelas">Kelas: <?=kelas($row['kelas'])?> </label></td>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: <?=$row['alamat']?> </label></td>
                </tr>
                <tr>
                    <td><label for="no_telepon">No Telepon: <?=$row['no_telepon']?> </label></td>
                </tr>
                <tr>
                    <td><a href="index.php">Kembali</td>
                </tr>
            </table>

        </fieldset>
    </form>
    </body>
</html>