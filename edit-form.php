<?php

include("config.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: index.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM siswa WHERE id=$id";
$query = mysqli_query($connect, $sql);
$row = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Belajar CRUD</title>
</head>

<body>
    <h3>Edit Data</h3>
  
    <form action="update-proses.php" method="POST">

        <fieldset>

            <table>
                <input type="hidden" name="id" value=<?= $row['id'] ?> />
                <tr>
                    <td><label for="nisn_siswa">NISN: </label></td>
                    <td><input type="text" name="nisn_siswa" id="nisn_siswa" placeholder="nisn_siswa" value=<?= $row['nisn'] ?> /></td>
                </tr>
             
                <tr>
                    <td><label for="nis">NIS: </label></td>
                    <td><input type="text" name="nis" id="nis" placeholder="nis" value=<?= $row['nis'] ?>></td>
                </tr>
                <tr>
                    <td><label for="nama">Nama: </label></td>
                    <td><input type="text" name="nama" id="nama" placeholder="nama" value=<?= $row['nama'] ?> /></td>
                </tr>
                <tr>
                    <td><label for="kelas">Kelas: </label></td>
                    <td>
                        <select name="kelas" id="kelas">
                            <option value="0" <?php if($row['kelas'] = 0 ) {echo 'selected';}?>>X RPL</option>
                            <option value="1" <?php if($row['kelas'] = 1 ) {echo 'selected';}?>>XI RPL</option>
                            <option value="2" <?php if($row['kelas'] = 2 ) {echo 'selected';}?>>XII RPL</option>
                        </select>
                    
                    </td>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: </label></td>
                    <td><textarea name="alamat" id="alamat"><?= $row['alamat'] ?></textarea></td>
                </tr>
                <tr>
                    <td><label for="telepon">Telepon: </label></td>
                    <td><input type="text" name="telepon" id="telepon" placeholder="telepon" value=<?= $row['no_telepon'] ?> /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="update" name="update" /></td>
                </tr>
            </table>
        </fieldset>
    </form>
    </body>
</html>