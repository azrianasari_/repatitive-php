<?php
include "config.php";

$result = mysqli_query($connect,"select*from siswa");

function kelas($data) {
    if($data == 0 ) {
        echo 'X RPL';
    }
    if($data == 1 ) {
        echo 'XI RPL';
    }
    if($data == 2 ) {
        echo 'XII RPL';
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Belajar CRUD</title>
 </head>
<body>

<h3>List Data</h3>
<?php 

if(isset($_GET['status'])){
   echo '<h3> '.$_GET['status'].' </h3>';
}

?>

<a href="tambah-form.php">[+] Tambah Baru</a>

<table border="1" cellpadding="8" cellspacing="0">
        <thead>
            <th>NO</th>
            <th>NISN</th>
            <th>NIS</th>
            <th>Nama</th>
            <th>Kelas</th>
            <th></th>
        </thead>
        <tbody>

        <?php 
            $index = 1;      
        ?>
      
        <?php while ($row = mysqli_fetch_array($result)) { ?>
            <tr>
                <td><?= $index++ ?></td>   
                <td><?= $row['nisn'] ?></td>
                <td><?= $row['nis'] ?></td>
                <td><?= $row['nama'] ?></td>
                <td><?= kelas($row['kelas']) ?></td>
                <td>
                    <a href='edit-form.php?id="<?= $row['id'] ?>"'>Edit</a> | 
                    <a href='hapus-proses.php?id=<?= $row['id'] ?>' onClick="return confirm('yakin hapus ?')">Hapus</a> |
                    <a href='detail-form.php?id="<?= $row['id'] ?>"'>Detail</a> 
                </td>
            </tr>
        <?php } ?>  
      
      </tbody>
</table>
    
</body>
</html>
