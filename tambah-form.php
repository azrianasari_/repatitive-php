<!DOCTYPE html>
<html>
<head>
    <title>Belajar CRUD</title>
</head>

<body>
    <h3>Tambah Data</h3>
 
    <form action="tambah-proses.php" method="POST">

        <fieldset>

            <table>
                <tr>
                    <td><label for="nisn_siswa">NISN: </label></td>
                    <td><input type="text" name="nisn_siswa" id="nisn_siswa" placeholder="nisn_siswa" /></td>
                </tr>
             
                <tr>
                    <td><label for="nis">NIS: </label></td>
                    <td><input type="text" name="nis" id="nis" placeholder="nis" /></td>
                </tr>
                <tr>
                    <td><label for="nama">Nama: </label></td>
                    <td><input type="text" name="nama" id="nama" placeholder="nama" /></td>
                </tr>
                <tr>
                    <td><label for="kelas">Kelas: </label></td>
                    <td>
                        <select name="kelas" id="kelas">
                            <option value="0">X RPL</option>
                            <option value="1">XI RPL</option>
                            <option value="2">XII RPL</option>
                        </select>
                    
                    </td>
                </tr>
                <tr>
                    <td><label for="alamat">Alamat: </label></td>
                    <td><textarea name="alamat" id="alamat"></textarea></td>
                </tr>
                <tr>
                    <td><label for="telepon">Telepon: </label></td>
                    <td><input type="text" name="telepon" id="telepon" placeholder="telepon" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="tambah" name="tambah" /></td>
                </tr>
            </table>

        </fieldset>

    </form>

    </body>
</html>